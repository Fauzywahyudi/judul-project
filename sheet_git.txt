mastergit init 
# inisialisasi git 

git add nama_file.txt 
#  menambahkan file ke kenangan, tapi belum dicatat kenangannya

git commit - m "first commit" 
#untuk menambahkan semua perubahan ke dalam kenangan dan di simpan

git log
# untuk melihat semua catatan kenangan

git diff
# untuk melihat perubahan sebelum file di add dan di commit

git checkout nama_branch
#untuk pindah dan  melihat salinan yang ada pada branch

git checkout 9389f(lima digit awal commit)
#untuk melihat kenangan pada masa lalu

git branch -b nama_branch
# untuk membuat cabang atau salinan baru

git branch -D nama_branch
# untuk menghapus nama branch atau salinan

git merge nama_branch
# untuk menggabungkan branch1 ke branch posisi terkini

git branch
#melihat semua branch

git remote -v
#melihat informasi yang di remote

git remote nama_anda alamat_repository_project
# configurasi remote sebelum mengupload ke gitlab

git push nama_anda nama_branch
# untuk mengupload git ke gitlab

git pull nama_remote nama_branch
#mengambil file dari web ke laptop/local

git stash
#klau eror pull

